package com.teradata.test.service;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class HelloServiceTest {

    @Test
    public void helloTest() {
        HelloService helloService = new HelloService();
        String response = helloService.hello("World");
        assertEquals("Hello World", response);
    }
}
