package com.teradata.test.service;

import org.springframework.stereotype.Service;

@Service
public final class HelloService {

    public String hello(String name) {
        return "Hello " + name;
    }
}