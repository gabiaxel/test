package com.teradata.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableAutoConfiguration
@SpringBootApplication
@EnableSwagger2
public class MicroserviceConfiguration {

    public static void main(String[] args) throws Exception {
        SpringApplication.run(MicroserviceConfiguration.class, args);
    }
}